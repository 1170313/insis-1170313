-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 14-Abr-2018 às 16:11
-- Versão do servidor: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insis`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `campanha`
--

CREATE TABLE `campanha` (
  `id` int(11) NOT NULL,
  `identificacao` varchar(255) NOT NULL,
  `objetivo` varchar(255) NOT NULL,
  `conteudo` varchar(255) NOT NULL,
  `canais` varchar(255) NOT NULL,
  `idade` varchar(255) NOT NULL,
  `pais` varchar(255) NOT NULL,
  `pais_2` varchar(255) NOT NULL,
  `timezone` varchar(255) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `timetosend` varchar(255) NOT NULL,
  `sucesso_envio` varchar(255) NOT NULL,
  `falha_envio` varchar(255) NOT NULL,
  `timestamp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `destinatarios`
--

CREATE TABLE `destinatarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `localidade` varchar(255) NOT NULL,
  `pais` varchar(255) NOT NULL,
  `moeda` varchar(255) NOT NULL,
  `lingua` varchar(255) NOT NULL,
  `canais` varchar(255) NOT NULL,
  `timestamp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `destinatarios`
--

INSERT INTO `destinatarios` (`id`, `nome`, `email`, `localidade`, `pais`, `moeda`, `lingua`, `canais`, `timestamp`) VALUES
(3, 'Nuno Neto', 'nunoneto17@gmail.com', 'Porto', 'pt', 'Euro', 'Portugues', 'email', '2018-04-12 01:27:56'),
(4, 'Andre Rocha', 'andrerocha171717@gmail.com', 'Portoa', 'pt', 'Euro', 'Portugues', 'email', '2018-04-12 01:28:05'),
(5, 'Outro', '1170313@isep.ipp.pt', 'Porto', 'ar', 'Euro', 'Portugues', 'email', '2018-04-12 01:28:19');

-- --------------------------------------------------------

--
-- Estrutura da tabela `timezone`
--

CREATE TABLE `timezone` (
  `id` int(11) NOT NULL,
  `campanha` varchar(255) NOT NULL,
  `pais` varchar(10) NOT NULL,
  `timezone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `campanha`
--
ALTER TABLE `campanha`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `destinatarios`
--
ALTER TABLE `destinatarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timezone`
--
ALTER TABLE `timezone`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `campanha`
--
ALTER TABLE `campanha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `destinatarios`
--
ALTER TABLE `destinatarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `timezone`
--
ALTER TABLE `timezone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
