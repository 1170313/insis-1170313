![alt text](http://www.fc.up.pt/fotocatgraf/img/logos/logo_ISEP.jpg)

# INSIS 2017/2018 - Nuno Neto 1170313

### Projeto 1
#### Tema
- Publicidade digital
#### Descrição de negócio
Pretende-se enviar conteúdo publicitário:
- (i) para um conjunto de destinatários diversos (e.g. localidade+país, (moeda,)
língua, canais disponíveis/autorizados);
- (ii) através de canais de envio (e.g. email, sms, redes sociais, etc.) escolhidos para a
campanha;
- (iii) com mensagem configurada segundo o perfil do destinatário.
#### Requisitos funcionais
O processo de envio duma campanha inclui o seguinte processo:
- Departamento comercial, dá início à campanha, caraterizando-a (i.e. identificação,
objetivo);
- Departamento de marketing, define os conteúdos a enviar por canal;
- Departamento de clientes, seleciona o(s) canal(canais) e alguns parâmetros do perfil de
cliente destinatário (e.g. país:UK, idade:>=25);
- Departamento comercial, (i) cancela a campanha, (ii) envia para revisão de um ou dos
dois passos anteriores, ou (iii) confirma a campanha, definindo o período de envio
(máximo 30 minutos);
- Departamento técnico, envia a campanha em função do canal definido e período de
envio definido (interpretado no fuso horário do destinatário).
#### Requisitos não funcionais
- Devem ser desenvolvidos e usados dois serviços web (REST, de preferência) que
disponibilizem a seguinte API:
-- POST: campanha; para criação duma nova campanha
-- GET: campanha/{x}; para leitura duma campanha
-- PUT: campanha/{x}; para atualização do estado da campanha
-- GET: campanha/{x}/conteúdo/canal/{y}; para leitura de informação do conteúdo por
canal
-- GET: destinatário/{x}; para leitura de informação sobre o destinatário
-- Outro justificadamente necessário
- Os erros e sucessos de cada campanha devem ser registados numa base de dados não
local, com informação suficiente para relacionar com a campanha, o canal e o país do
destinatário, mas não com o destinatário (cf. RGPD).
- Os sistemas de canal de envio têm capacidade limitada (definida por configuração local)
que não ultrapassa 1 mensagem/5 segundos.
- Sempre que existam conetores disponíveis, devem ser usados sistemas diferentes para
cada tipo de canal (e.g. gmail e mailchimp).
- Devem ser justificadamente usadas ferramentas de integração de sistemas, como ESB,
BPS e Message Broker.
- Devem ser adotados e justificados padrões/estilos/boas práticas de desenvolvimento de
software.
- Mais informação sobre o projeto será disponibilizada durante as aulas.
#### Condições
- Realizado em grupo de 2 estudantes
- Avaliação do trabalho global (de grupo): 30%
- Avaliação individual: 70%
- P1.1: 30%
-- Processo de negócio
-- Serviços web
- P1.2: 70%
-- Melhoria de P1.1
-- Integração dos Sistemas de Software