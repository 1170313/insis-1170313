var express = require('express');
var router = express.Router();
var http = require('http');
var mysql = require('mysql');
var request = require('request');
var moment = require('moment');

var connection = mysql.createConnection({
    host: process.env.DATABASE_HOST || '127.0.0.1',
    user: "insis",
    password: "insis", // minha pass para local
    database: "insis"
});

router.get('/', function (req, res) {
    res.render('index.html');
});

router.get('/marketing/:campanha/:id', function (req, res) {
    console.log('http://18.130.69.231:9004/links/' + req.params.campanha + '/' + req.params.id+'?agent='+req.query.agent);
    request('http://18.130.69.231:9004/links/' + req.params.campanha + '/' + req.params.id+'?agent='+req.query.agent, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log("ok");
        }
        });

    connection.query('select * from campanha where identificacao = ?', [req.params.campanha], function (err, data) {
        if (err)
            console.log(err);
        else
            var email = decrypt(req.params.id);
        connection.query('select * from destinatarios where email = ?', [email], function (err, user) {

            if (err)
                console.log(err);
            else

            res.render('marketing', {
                info: data,
                user: user
            });
        });

    });
});

router.route('/campanha')
    /**
     * @api {get} /campanha Campanha
     * @apiGroup Campanha
     *
     * @apiSuccess {String} status Ver as campanhas
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *    {
     *    "Data": [
     *      {
     *        "id": 1,
     *        "identificacao": "001",
     *        "objetivo": "Teste Insis",
     *        "conteudo": "Tarifarios Moveis",
     *        "canais": "SMS",
     *        "idade": "Mais de 25",
     *        "pais": "pt",
     *        "estado": "Enviado",
     *        "timetosend": "",
     *        "timestamp": "2018-03-18 01:31:55"
     *      }
     *    ]
     *  }
     *
     */
    .get(function (req, res) {
        var data = {
            "Data": ""
        };

        connection.query("DELETE FROM destinatarios", function (err, result) {
            if (err) throw err;
            console.log("Deleted old destinatarios: " + result.affectedRows);
        });

        request('http://18.130.69.231:9002/destinatario', function (error, response, body) {
            if (!error && response.statusCode == 200) {

                var dest = JSON.parse(body);
                for (let index = 0; index < dest.length; index++) {
                    var email = dest[index].email;
                    var ref_unica = encrypt(dest[index].email);
                    console.log("MAIL: "+email);
                    console.log("REF_UNICA"+ref_unica);
                    connection.query("SELECT * from destinatarios where email = ?", [email], function (err, rows, fields) {
                        if (rows.length > 0) {
                            // se ja existe, nada faz
                        } else {
                            connection.query("INSERT INTO destinatarios (chave, nome, email, ref_unica, localidade, pais, moeda, lingua, canais, timestamp) VALUES (?,?,?,?,?,?,?,?,?,?)", [dest[index].id,dest[index].nome, dest[index].email, encrypt(dest[index].email), dest[index].localidade, dest[index].pais, dest[index].moeda, dest[index].lingua, dest[index].canais, dest[index].timestamp], function (err, result) {
                                if (err) throw err;
                                console.log("Number of records inserted: " + result.affectedRows);
                            });
                        }
                    });
                }
                //console.log(dest[3]);
                //console.log(body) // Print the google web page.
            }
        })

        connection.query("SELECT * from campanha", function (err, rows, fields) {
            if (rows != undefined) {
                data["Data"] = rows;
                res.json(data);
            } else {
                res.status(404) // HTTP status 404: NotFound
                    .send('Not found');
            }
        });
    }) /* Não era pedido, mas por lógica seria interessante ter a info de todas as campanhas*/
    /**
     * @api {post} /campanha Campanha
     * @apiGroup Campanha
     *
     * @apiSuccess {String} status Criar uma campanha
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *    {
     *      "status": "Criado."
     *    }
     *
     */
    .post(function (req, res) {
        var identificacao = req.body.identificacao == undefined ? "" : req.body.identificacao;
        var objetivo = req.body.objetivo == undefined ? "" : req.body.objetivo;
        var conteudo = req.body.conteudo == undefined ? "" : req.body.conteudo;
        var canais = req.body.canais == undefined ? "" : req.body.canais;
        var idade = req.body.idade == undefined ? "" : req.body.idade;
        var pais = req.body.pais == undefined ? "" : req.body.pais;
        var pais_2 = req.body.pais_2 == undefined ? "" : req.body.pais_2;
        var estado = req.body.estado == undefined ? "" : req.body.estado;
        var timetosend = req.body.timetosend == undefined ? "" : req.body.timetosend;
        var mysqlTimestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
        var timezone = "";
        var sucesso_envio = "0";
        var falha_envio = "0";
        connection.query("INSERT INTO campanha (identificacao, objetivo, conteudo, canais, idade, pais, pais_2, timezone, estado, timetosend, sucesso_envio, falha_envio,timestamp) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", [identificacao, objetivo, conteudo, canais, idade, pais, pais_2, timezone, estado, timetosend, sucesso_envio, falha_envio, mysqlTimestamp], function (err, result) {
            if (err) throw err;
            console.log("Number of records inserted: " + result.affectedRows);
            res.json({
                "status": "Criado."
            });
        });
    });
/* Pedido e implementado */

router.route('/campanha/:identificador')
    /**
     * @api {get} /campanha/:identificador Campanha com identificador
     * @apiGroup Campanha
     *
     * @apiSuccess {String} status Ver as campanhas
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *      {
     *        "id": 1,
     *        "identificacao": "001",
     *        "objetivo": "Teste Insis",
     *        "conteudo": "Tarifarios Moveis",
     *        "canais": "SMS",
     *        "idade": "Mais de 25",
     *        "pais": "pt",
     *        "estado": "Enviado",
     *        "timetosend": "",
     *        "timestamp": "2018-03-18 01:31:55"
     *      }
     *
     */
    .get(function (req, res) {
        var sql = 'SELECT * from campanha WHERE identificacao = ?';
        var id = req.params.identificador;
        connection.query(sql, [id], function (err, result) {
            if (err) throw err;
            if (result.length > 0) {
                res.json(result);
            } else {
                res.status(404) // HTTP status 404: NotFound
                    .send('Not found');
            }

        });
    }) /* Pedido e implementado */
    /**
     * @api {delete} /campanha/:identificador Apagar uma campanha
     * @apiGroup Campanha
     *
     * @apiSuccess {String} status Apagado
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *    {
     *      "status": "Apagado."
     *    }
     *
     */
    .delete(function (req, res) {
        var sql = 'DELETE from campanha WHERE identificacao = ?';
        var id = req.params.identificador;
        connection.query(sql, [id], function (err, result) {
            if (err) throw err;
            res.json({
                "status": "Apagado."
            });
        });
    }) /* Não era pedido, mas para o caso de cancelar pode ser apagado ou não, questão a discutir */
    /**
     * @api {put} /campanha/:identificador Atualizar dados de campanha
     * @apiGroup Campanha
     *
     * @apiSuccess {String} status 1 record(s) updated
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *    {
     *      "status": "1 record(s) updated"
     *    }
     *
     */
    .put(function (req, res) {

        var sql = 'SELECT * from campanha WHERE identificacao = ?';
        var id = req.params.identificador;
        var result_search;

        var query = connection.query(sql, [id], function (err, result) {
            if (err) throw err;
            result_search = result;
            console.log(result);
        });
        query
            .on('end', function () {
                var id_campanha = result_search[0].id;
                var identificacao = req.body.identificacao == undefined ? result_search[0].identificacao : req.body.identificacao;
                var objetivo = req.body.objetivo == undefined ? result_search[0].objetivo : req.body.objetivo;
                var conteudo = req.body.conteudo == undefined ? result_search[0].conteudo : req.body.conteudo;
                var canais = req.body.canais == undefined ? result_search[0].canais : req.body.canais;
                var idade = req.body.idade == undefined ? result_search[0].idade : req.body.idade;
                var pais = req.body.pais == undefined ? result_search[0].pais : req.body.pais;
                var pais_2 = req.body.pais_2 == undefined ? result_search[0].pais_2 : req.body.pais_2;
                var estado = req.body.estado == undefined ? result_search[0].estado : req.body.estado;
                var hora_envio = req.body.hora_envio == undefined ? "" : req.body.hora_envio; //2018-04-12T15:05:00+00:00
                var timetosend = req.body.timetosend == undefined ? result_search[0].timetosend : req.body.timetosend;
                var successo_envio = req.body.sucesso_envio == undefined ? result_search[0].sucesso_envio : req.body.sucesso_envio;
                var falha_envio = req.body.falha_envio == undefined ? result_search[0].falha_envio : req.body.falha_envio;

                hora_envio = new Date(hora_envio);


                if (req.body.sucesso_envio) {
                    console.log("OLD " + result_search[0].sucesso_envio);
                    console.log("PUT " + req.body.sucesso_envio);
                    var temp = parseInt(req.body.sucesso_envio) + parseInt(result_search[0].sucesso_envio);
                    successo_envio = temp;
                    console.log("NEW " + successo_envio);
                }
                if (req.body.falha_envio) {
                    falha_envio = result_search[i].falha_envio + req.body.falha_envio;
                }

                if (req.body.pais) {
                    var timezone = "";
                    connection.query("INSERT INTO timezone (campanha, pais, timezone) VALUES (?,?,?)", [identificacao, pais, timezone], function (err, result) {
                        if (err) throw err;
                    });
                }
                if (req.body.pais_2) {
                    var timezone = "";
                    connection.query("INSERT INTO timezone (campanha, pais,timezone) VALUES (?,?,?)", [identificacao, pais_2, timezone], function (err, result) {
                        if (err) throw err;
                    });

                }

                if (req.body.timetosend) {
                    var hora_final = new Date(hora_envio.getTime() + timetosend * 60000).toISOString().split('.')[0];
                    console.log(pais);
                    console.log(hora_envio);
                    console.log(hora_final);
                    var timezone;
                    if (pais == "pt") {
                        timezone = new Date(hora_envio.getTime() + 60 * 60 * 1000).toISOString().split('.')[0]; // hora * 60 * 60
                        var sql = "UPDATE timezone SET timezone = ? WHERE campanha  = ? and pais = ?";
                        var query_update = connection.query(sql, [timezone, id, pais], function (err, result) {
                            if (err) throw err;
                            console.log(result.affectedRows + " record(s) updated");

                        });
                    } else {
                        timezone = new Date(hora_envio.getTime() + 61 * 60 * 1000).toISOString().split('.')[0];
                        var sql = "UPDATE timezone SET timezone = ? WHERE campanha  = ? and pais = ?";
                        var query_update = connection.query(sql, [timezone, id, pais_2], function (err, result) {
                            if (err) throw err;
                            console.log(result.affectedRows + " record(s) updated");
                        });
                    };

                    if (pais_2 == "pt") {
                        timezone = new Date(hora_envio.getTime() + 60 * 60 * 1000).toISOString().split('.')[0]; // hora * 60 * 60
                        var sql = "UPDATE timezone SET timezone = ? WHERE campanha  = ? and pais = ?";
                        var query_update = connection.query(sql, [timezone, id, pais_2], function (err, result) {
                            if (err) throw err;
                            console.log(result.affectedRows + " record(s) updated");
                        });
                    } else {
                        timezone = new Date(hora_envio.getTime() + 61 * 60 * 1000).toISOString().split('.')[0];
                        var sql = "UPDATE timezone SET timezone = ? WHERE campanha  = ? and pais = ?";
                        var query_update = connection.query(sql, [timezone, id, pais_2], function (err, result) {
                            if (err) throw err;
                            console.log(result.affectedRows + " record(s) updated");
                        });
                    }

                }


                var mysqlTimestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');

                var sql = "UPDATE campanha SET identificacao = ?, objetivo = ?, conteudo = ?, canais = ?, idade = ?,pais = ?,pais_2 = ?, estado = ?, timetosend = ?, sucesso_envio = ?, falha_envio = ?,timestamp = ? WHERE id  = ?";
                query_update = connection.query(sql, [identificacao, objetivo, conteudo, canais, idade, pais, pais_2, estado, timetosend, successo_envio, falha_envio, mysqlTimestamp, id_campanha], function (err, result) {
                    if (err) throw err;

                });

                res.json(({
                    "status": "record(s) updated"
                }));
            });

    });
/* Pedido e implementado */

router.route('/campanha/:identificador/destinatarios')
    .get(function (req, res) {
        var sql = 'SELECT DISTINCT identificacao,objetivo,conteudo,c.canais,nome,email,ref_unica,d.pais from campanha as c RIGHT JOIN destinatarios as d ON identificacao = ? WHERE c.canais = d.canais and (c.pais_2 = d.pais or c.pais = d.pais)';
        var id = req.params.identificador;
        var data = {
            "result": ""
        };
        connection.query(sql, [id], function (err, result) {
            if (err) throw err;
            data["result"] = result;
            res.json(data);
        });
    }); /* Pedido e implementado */

router.route('/campanha/:identificador/destinatarios/:limit')
    .get(function (req, res) {
        var sql = 'SELECT DISTINCT identificacao,objetivo,conteudo,c.canais,nome,email,ref_unica,d.pais from campanha as c RIGHT JOIN destinatarios as d ON identificacao = ? WHERE c.canais = d.canais and (c.pais_2 = d.pais or c.pais = d.pais) LIMIT ?';
        var id = req.params.identificador;
        var limitador = parseInt(req.params.limit);
        console.log(limitador);
        var data = {
            "result": ""
        };
        connection.query(sql, [id, limitador], function (err, result) {
            if (err) throw err;
            data["result"] = result;
            res.json(data);
        });
    }); /* Pedido e implementado */


router.route('/campanha/:identificador/conteudo/:canal') /* como estava não fazia sentido e violava as regras de REST API */
    /**
     * @api {get} /campanha/:identificador/conteudo/:canal Conteudo de Canal por identificador de campanha
     * @apiGroup Campanha
     *
     * @apiSuccess {String} status Conteudo de Canal por identificador de campanha
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *    {
     *        "conteudo": "Tarifarios Moveis",
     *    }
     *
     */
    .get(function (req, res) {
        var sql = 'SELECT * from campanha WHERE identificacao = ? and canais = ? ';
        var id = req.params.identificador;
        var canal2 = req.params.canal;
        connection.query(sql, [id, canal2], function (err, result) {
            if (err) throw err;
            res.json({
                "conteudo": result[0].conteudo
            });
        });
    });
/* Pedido e implementado */

router.route('/campanha/:identificador/timezone')
    .get(function (req, res) {

        var sql = 'SELECT pais,timezone from timezone WHERE campanha = ? ORDER BY timezone';
        var id = req.params.identificador;
        var data = {
            "result": ""
        };

        connection.query(sql, [id], function (err, result) {
            if (result.length != 0) {
                data["result"] = result;
                data.completo = "Nao";
                data.count = result.length;
                res.json(data);
            } else {
                data.completo = "Sim";
                data.count = result.length;
                res.json(data);
            }

        })
    });

router.route('/campanha/:identificador/timezone/:tz')
    .delete(function (req, res) {
        var sql = 'DELETE from timezone WHERE campanha = ? and pais = ?';
        var id = req.params.identificador;
        var tz = req.params.tz;
        var data = {
            "result": ""
        };

        connection.query(sql, [id, tz], function (err, result) {});
        var sql = 'SELECT pais,timezone from timezone WHERE campanha = ? ORDER BY timezone';
        var id = req.params.identificador;
        var data = {
            "result": ""
        };

        connection.query(sql, [id], function (err, result) {
            if (result.length != 0) {
                data["result"] = result;
                data.completo = "Nao";
                data.count = result.length;
                res.json(data);
            } else {
                data.completo = "Sim";
                data.count = result.length;
                res.json(data);
            }

        })

    }); /* Não era pedido, mas para o caso de cancelar pode ser apagado ou não, questão a discutir */

/* Verificar os destinatarios que existam, desde que não seja em public release devido a dados sensiveis */

// Nodejs encryption with CTR
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';

function encrypt(text) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text) {
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}
module.exports = router;