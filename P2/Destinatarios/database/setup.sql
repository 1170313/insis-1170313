CREATE TABLE destinatarios (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nome varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  ref_unica varchar(255) NOT NULL,
  localidade varchar(255) NOT NULL,
  pais varchar(255) NOT NULL,
  moeda varchar(255) NOT NULL,
  lingua varchar(255) NOT NULL,
  canais varchar(255) NOT NULL,
  timestamp varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO destinatarios (nome,email,ref_unica,localidade,pais,moeda,lingua,canais,timestamp) VALUES
("Nuno Neto","nunoneto17@gmail.com","0a9bda9f2f746c7f611ec40affe21597fd4c8bb9","Portugal","pt","Euro","Portugues","Email",now());

INSERT INTO destinatarios (nome,email,ref_unica,localidade,pais,moeda,lingua,canais,timestamp) VALUES
("Andre Rocha","andrerocha171717@gmail.com","0580d082246377733848b55aa3b44dcc934889b5d9eb93677ba4","Espanha","es","Euro","Espanhol","Email",now());
