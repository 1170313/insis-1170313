var express = require('express');
var device = require('express-device');
var router = express.Router();
var http = require('http');
var mysql = require('mysql');
var request = require('request');
var moment = require('moment');
var connection = mysql.createConnection({
    host: process.env.DATABASE_HOST || '127.0.0.1',
    user: "insis",
    password: "insis", // minha pass para local
    database: "insis"
});

router.route('/reserva')
    /**
     * @api {get} /links Links
     * @apiGroup Links
     *
     * @apiSuccess {String} status Ver os links acedidos
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *    {
     *    "Data": [
     *      {
     *        "id": 1,
     *        "campanha": "insis01",
     *        "ref_unica": "o3ajvi2naovjaod",
     *        "destinatario": "nunoneto17@gmail.com",
     *        "agent": "pt"
     *      }
     *    ]
     *  }
     *
     */
    .get(function (req, res) {
        var data = {
            "Data": ""
        };
        if (req.query.schedule) {
            if (req.query.timetosend && req.query.canal) {
                var get_schedule = new Date(req.query.schedule)
                var hora_envio = new Date(get_schedule.getTime() + 60 * 60000);
                var hora_final_envio = new Date(hora_envio.getTime() + req.query.timetosend * 60000).toISOString().split('.')[0];
                hora_envio = hora_envio.toISOString().split('.')[0];
                console.log(hora_envio);
                console.log(hora_final_envio);
                //WHERE created_at BETWEEN '2011-12-01' AND '2011-12-07'
                connection.query("SELECT * from reserva where canal = ? AND schedule BETWEEN ? AND ?", [req.query.canal, hora_envio, hora_final_envio], function (err, rows, fields) {
                    if (err) throw err;
                    if (rows.length > 0) {
                        data["Data"] = rows;
                        var object = {
                            disponivel: "conflito",
                        };
                        res.json(object).status(409);
                    } else {
                        var object = {
                            disponivel: "disponivel",
                        };
                        res.json(object).status(200);
                    }
                });
            } else {
                var object = {
                    disponivel: "missing parameters",
                };
                res.json(object).status(400);
            }
        } else {
            var object = {
                disponivel: "missing parameters",
            };
            res.json(object).status(400);
        }
    }) /* Não era pedido, mas por lógica seria interessante ter a info de todas as campanhas*/
    /**
     * @api {post} /links Links
     * @apiGroup Links
     *
     * @apiSuccess {String} status Adicionar um link de campanha
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *    {
     *      "status": "Criado."
     *    }
     *
     */
    .post(function (req, res) {
        var campanha = req.body.campanha;
        var canal = req.body.canal;
        var schedule = req.body.schedule;
        var timetosend = req.body.timetosend

        var object = {
            campanha: campanha,
            canal: canal,
            schedule: schedule,
            timetosend: timetosend
        };

        connection.query("INSERT INTO reserva (campanha, canal, schedule, timetosend) VALUES (?,?,?,?)", [campanha, canal, schedule, timetosend], function (err, result) {
            if (err) throw err;
            console.log("Number of records inserted: " + result.affectedRows);
            res.json(object).status(201);
        });
    });
/* Pedido e implementado */

module.exports = router;