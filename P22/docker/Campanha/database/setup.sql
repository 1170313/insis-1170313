CREATE TABLE campanha (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
identificacao TEXT,
objetivo TEXT,
conteudo TEXT,
canais TEXT,
idade TEXT,
pais TEXT,
pais_2 TEXT,
timezone TEXT,
estado TEXT,
timetosend TEXT,
sucesso_envio TEXT,
falha_envio TEXT,
timestamp TEXT
);

CREATE TABLE destinatarios (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  chave varchar(255) NOT NULL,
  nome varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  ref_unica varchar(255) NOT NULL,
  localidade varchar(255) NOT NULL,
  pais varchar(255) NOT NULL,
  moeda varchar(255) NOT NULL,
  lingua varchar(255) NOT NULL,
  canais varchar(255) NOT NULL,
  timestamp varchar(255) NOT NULL
);

CREATE TABLE timezone (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  campanha varchar(255) NOT NULL,
  pais varchar(10) NOT NULL,
  timezone varchar(255) NOT NULL
);