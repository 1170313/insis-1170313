var express = require('express');
var router = express.Router();
var http = require('http');
var mysql = require('mysql');
var request = require('request');
var moment = require('moment');

var connection = mysql.createConnection({
    host: process.env.DATABASE_HOST || '127.0.0.1',
    user: "insis",
    password: "insis", // minha pass para local
    database: "insis"
});

router.get('/', function (req, res) {
    res.render('index.html');
});


router.route('/destinatario/:identificador')
/**
 * @api {get} /destinatario/:identificador Um determinado destinatário
 * @apiGroup Destinatario
 *
 * @apiSuccess {String} status Um determinado destinatário
 *
 * @apiSuccessExample {json} Sucesso
 *    HTTP/1.1 200 OK
 *    {
   *      "id": 1,
   *      "nome": "Nuno",
   *      "email": "nunoneto17@gmail.com",
   *      "localidade": "Porto",
   *      "pais": "Portugal",
   *      "moeda": "Euro",
   *      "lingua": "Portugues",
   *      "canais": "Email",
   *      "timestamp": ""
   *    }
 *
 */
    .get(function (req, res) {
        var sql = 'SELECT * from destinatarios WHERE id = ?';
        var id = req.params.identificador;
        connection.query(sql, [id], function (err, result) {
            if (err) throw err;
            if (result.length != 0) {
                res.render('profile', {
                    info: result
                });
            } else {
                res.json("Não existe destinatário com esse id.");
            }
        });
    });
/* Pedido e implementado */

router.route('/destinatario/:identificador/update')
    .get(function (req, res) {
        console.log(req.query.myselect);
        var sql = "UPDATE destinatarios SET canais = ? WHERE id  = ?";
        var query_update = connection.query(sql, [req.query.myselect, req.params.identificador], function (err, result) {
            if (err) throw err;
            console.log(result.affectedRows + " record(s) updated");
            res.redirect('/destinatario/'+req.params.identificador);
        });
    });
/* Pedido e implementado */

router.route('/destinatario')
/**
 * @api {get} /destinatario/:identificador Destinatarios
 * @apiGroup Destinatario
 *
 * @apiSuccess {String} status Um determinado destinatário
 *
 * @apiSuccessExample {json} Sucesso
 *    HTTP/1.1 200 OK
 *    {
   *      "id": 1,
   *      "nome": "Nuno",
   *      "email": "nunoneto17@gmail.com",
   *      "localidade": "Porto",
   *      "pais": "Portugal",
   *      "moeda": "Euro",
   *      "lingua": "Portugues",
   *      "canais": "Email",
   *      "timestamp": ""
   *    }
 *
 */
    .get(function (req, res) {
        var sql = 'SELECT * from destinatarios';
        connection.query(sql, function (err, result) {
            if (err) throw err;
            res.json(result);
        });
    }) /* Verificar os destinatarios que existam, desde que não seja em public release devido a dados sensiveis */
    /**
     * @api {post} /destinatario Criar um destinatario
     * @apiGroup Destinatario
     *
     * @apiSuccess {String} status Criado
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *    {
   *      "status": "Criado."
   *    }
     *
     */
    .post(function (req, res) {
        var nome = req.body.nome == undefined ? "" : req.body.nome;
        var email = req.body.email == undefined ? "" : req.body.email;
        var localidade = req.body.localidade == undefined ? "" : req.body.localidade;
        var pais = req.body.pais == undefined ? "" : req.body.pais;
        var moeda = req.body.moeda == undefined ? "" : req.body.moeda;
        var lingua = req.body.lingua == undefined ? "" : req.body.lingua;
        var canais = req.body.canais == undefined ? "" : req.body.canais;
        var mysqlTimestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');

        connection.query("INSERT INTO destinatarios (nome, email, localidade, pais, moeda, lingua, canais, timestamp) VALUES (?,?,?,?,?,?,?,?)", [nome, email, localidade, pais, moeda, lingua, canais, mysqlTimestamp], function (err, result) {
            if (err) throw err;
            console.log("Number of records inserted: " + result.affectedRows);
            res.json({
                "status": "Criado"
            });
        });
    });
/* Necessidade de criar um destinatario na API */


// Nodejs encryption with CTR
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';

function encrypt(text) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text) {
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}
module.exports = router;