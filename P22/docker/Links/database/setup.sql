CREATE TABLE links (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
campanha TEXT,
ref_unica TEXT,
destinatario TEXT,
agent TEXT,
timestamp TEXT
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
