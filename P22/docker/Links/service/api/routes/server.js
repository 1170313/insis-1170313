var express = require('express');
var device = require('express-device');
var router = express.Router();
var http = require('http');
var mysql = require('mysql');
var request = require('request');
var moment = require('moment');
var connection = mysql.createConnection({
    host: process.env.DATABASE_HOST || '127.0.0.1',
    user: "insis",
    password: "insis", // minha pass para local
    database: "insis"
});

router.route('/links')
    /**
     * @api {get} /links Links
     * @apiGroup Links
     *
     * @apiSuccess {String} status Ver os links acedidos
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *    {
     *    "Data": [
     *      {
     *        "id": 1,
     *        "campanha": "insis01",
     *        "ref_unica": "o3ajvi2naovjaod",
     *        "destinatario": "nunoneto17@gmail.com",
     *        "agent": "pt"
     *      }
     *    ]
     *  }
     *
     */
    .get(function (req, res) {
        var data = {
            "Data": ""
        };

        connection.query("SELECT * from links", function (err, rows, fields) {
            if (rows != undefined) {
                data["Data"] = rows;
                res.json(data);
            } else {
                res.status(404) // HTTP status 404: NotFound
                    .send('Not found');
            }
        });
    }) /* Não era pedido, mas por lógica seria interessante ter a info de todas as campanhas*/
    /**
     * @api {post} /links Links
     * @apiGroup Links
     *
     * @apiSuccess {String} status Adicionar um link de campanha
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *    {
     *      "status": "Criado."
     *    }
     *
     */
    .post(function (req, res) {
        var campanha = req.body.campanha == undefined ? "" : req.body.campanha;
        var ref_unica = req.body.ref_unica == undefined ? "" : req.body.ref_unica;
        var destinatario = req.body.destinatario == undefined ? "" : req.body.destinatario;
        var agent = req.body.agent == undefined ? "" : req.headers['user-agent'];
        var timestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');

        var object = {
            campanha : campanha,
            ref_unica : ref_unica,
            destinatario : destinatario,
            agent : agent,
            timestamp : timestamp
        };

        connection.query("INSERT INTO links (campanha, ref_unica, destinatario, agent, timestamp) VALUES (?,?,?,?,?)", [campanha, ref_unica, destinatario, agent, timestamp], function (err, result) {
            if (err) throw err;
            console.log("Number of records inserted: " + result.affectedRows);
            res.json(object).status(201);
        });
    });
/* Pedido e implementado */
router.route('/links/:campanha/:id')
.get(function (req, res) {
    console.log("camp: "+req.params.campanha);
    console.log("id: "+req.params.id);
    console.log("agent: "+req.query.agent);
    var agent = req.query.agent == undefined ? "" :req.query.agent;
    connection.query("UPDATE links set agent = ? where campanha = ? and ref_unica = ?", [agent,req.params.campanha,req.params.id], function (err, result) {
        if (err) throw err;
        console.log("Number of records updated: " + result.affectedRows);
        var object = {
            campanha : req.params.campanha,
            ref_unica : req.params.id,
            agent : agent,
        };
        res.json(object).status(200);
    });
});


module.exports = router;