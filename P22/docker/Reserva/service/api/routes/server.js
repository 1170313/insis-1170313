var express = require('express');
var amqp = require('amqplib/callback_api');
var device = require('express-device');
var router = express.Router();
var http = require('http');
var mysql = require('mysql');
var request = require('request');
var moment = require('moment');
var connection = mysql.createConnection({
    host: process.env.DATABASE_HOST || '127.0.0.1',
    user: "insis",
    password: "insis", // minha pass para local
    database: "insis"
});

/*var amqp = require('amqp');
var connection = amqp.createConnection({url: "amqp://guest:guest@18.130.69.231:5672"},{defaultExchangeName: ''});
connection.on('ready', function() {
  console.log('connected');
});*/

var amqpConn = null;
function start() {
  amqp.connect("amqp://18.130.69.231", function(err, conn) {
    if (err) {
      console.error("[AMQP]", err.message);
      return setTimeout(start, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] reconnecting");
      return setTimeout(start, 1000);
    });

    console.log("[AMQP] connected");
    amqpConn = conn;

  });
}
start();

router.route('/reserva')
    /**
     * @api {get} /links Links
     * @apiGroup Links
     *
     * @apiSuccess {String} status Ver os links acedidos
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *    {
     *    "Data": [
     *      {
     *        "id": 1,
     *        "campanha": "insis01",
     *        "ref_unica": "o3ajvi2naovjaod",
     *        "destinatario": "nunoneto17@gmail.com",
     *        "agent": "pt"
     *      }
     *    ]
     *  }
     *
     */
    .post(function (req, res) {

        var data = {
            "Data": ""
        };
        if (req.body.schedule) {
            if (req.body.timetosend && req.body.canal) {
                var get_schedule = new Date(req.body.schedule)
                var hora_envio = new Date(get_schedule.getTime() + 60 * 60000);
                var hora_final_envio = new Date(hora_envio.getTime() + req.body.timetosend * 60000).toISOString().split('.')[0];
                hora_envio = hora_envio.toISOString().split('.')[0];
                console.log(hora_envio);
                console.log(hora_final_envio);
                //WHERE created_at BETWEEN '2011-12-01' AND '2011-12-07'
                connection.query("SELECT * from reserva where canal = ? AND schedule BETWEEN ? AND ?", [req.body.canal, hora_envio, hora_final_envio], function (err, rows, fields) {
                    if (rows.length > 0) {
                        data["Data"] = rows;
                        var object = {
                            disponivel: "conflito",
                        };
                        res.json(object).status(409);
                        return;
                    } else {
                        var object1 = {
                            disponivel: "disponivel",
                        }

                        var campanha = req.body.identificacao;
                        var canal = req.body.canal;
                        var schedule = req.body.schedule;
                        var timetosend = req.body.timetosend

                        var object = {
                            campanha: campanha,
                            canal: canal,
                            schedule: schedule,
                            timetosend: timetosend
                        };
                        console.log(req.body);

                        connection.query("INSERT INTO reserva (campanha, canal, schedule, timetosend) VALUES (?,?,?,?)", [campanha, canal, schedule, timetosend], function (err, result) {
                            console.log("Number of records inserted: " + result.affectedRows);
                            //res.json(object).status(201);
                        });

                        amqpConn.createChannel(function(err, ch) {
                            var q = 'campanha_reserva'
                            //identificacao
                            //var msg = 'pipo';
                            var msg = JSON.stringify({"identificacao": campanha});
                        
                            ch.assertQueue(q, {durable: false});
                            // Note: on Node 6 Buffer.from(msg) should be used
                            ch.sendToQueue(q, new Buffer(msg));
                      
                        });

                        res.json(object1).status(200);

                    }
                });

            } else {
                var object = {
                    disponivel: "missing parameters",
                };
                res.json(object).status(400);
            }
        } else {
            var object = {
                disponivel: "missing parameters",
            };
            res.json(object).status(400);
        }
    }) /* Não era pedido, mas por lógica seria interessante ter a info de todas as campanhas*/
    /**
     * @api {post} /links Links
     * @apiGroup Links
     *
     * @apiSuccess {String} status Adicionar um link de campanha
     *
     * @apiSuccessExample {json} Sucesso
     *    HTTP/1.1 200 OK
     *    {
     *      "status": "Criado."
     *    }
     *
     */
    .get(function (req, res) {
        var sql = 'SELECT * from reserva';
        connection.query(sql, function (err, result) {
            if (err) throw err;
            res.json(result).status(200);
        });
    });
/* Pedido e implementado */

module.exports = router;