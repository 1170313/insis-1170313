#!/bin/sh
docker run -d -p 4369:4369 -p 5671:5671 -p5672:5672 -p15671:15671 -p15672:15672 -p25672:25672 --hostname my-rabbit --name some-rabbit rabbitmq:3-management
sleep 2
cd /home/ec2-user/docker/Campanha
docker-compose build
docker-compose up -d
sleep 2
cd /home/ec2-user/docker/Destinatarios
docker-compose build
docker-compose up -d
sleep 2
cd /home/ec2-user/docker/Links
docker-compose build
docker-compose up -d
sleep 2
cd /home/ec2-user/docker/Reserva
docker-compose build
docker-compose up -d
sleep 2
cd /home/ec2-user/docker/CampanhaQuery
docker-compose build
docker-compose up -d
sleep 2