#!/usr/bin/env node

var http = require('http');
var fs = require('fs');
var amqp = require('amqplib/callback_api');

console.log(" [*] Waiting for messages in %s. To exit press CTRL+C");
amqp.connect('amqp://18.130.69.231', function (err, conn) {
  conn.createChannel(function (err, ch) {
    var q = 'campanhas';
    ch.assertQueue(q, {
      durable: false
    });
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
    ch.consume(q, function (msg) {
      var identificacao = JSON.parse(msg.content.toString());
      console.log(" [x] Received %s", msg.content.toString());

      setTimeout(function() {
        console.log("[AMQP] Consome campanha criada");
        var post_data = JSON.stringify({
            'campanha': identificacao["identificacao"]
          });
        // An object of options to indicate where to post to
        //POST DA CAMPANHA PARA CAMPANHQ QUERY
        var post_options = {
          host: '18.130.69.231',
          port: '9101',
          path: '/campanha',
          method: 'POST',
          headers: {
            'Authorization': 'Basic YWRtaW46YWRtaW4=',
            'Content-Type': 'application/json'
          }
        };
  
        // Set up the request
        var post_req = http.request(post_options, function (res) {
          res.setEncoding('utf8');
          res.on('data', function (chunk) {
            console.log('Response: ' + chunk);
          });
        });
  
        // post the data
        console.log(post_data);
        post_req.write(post_data);
        post_req.end();
      }, 10000);

    }, {
      noAck: true
    });
  });
});