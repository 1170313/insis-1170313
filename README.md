![picture](deployment-diagram.png)
# Servidores alojados no AWS
## API Manager
Carbon: https://51.38.65.166:9443/carbon/

Publisher: https://51.38.65.166:9443/publisher/

Store: https://51.38.65.166:9443/store/

## API Analytics
Carbon: https://51.38.65.166:9444/carbon/


## RabbitMQ
http://18.130.69.231:15672
## WebService Campanha
http://18.130.69.231:9001
## WebService Destinatários
http://18.130.69.231:9002
## Microservice Reserva
http://18.130.69.231:9003
## Microservice Links
http://18.130.69.231:9004